package com.chebdev.m_servicedesk;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

// класс отображения списка всех заявок
public class RequestsListParentActivity extends TabActivity {

    // метод срабатываемый при создании активити
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.requests_list_parent_layout);
		// получаем TabHost
		TabHost tabHost = getTabHost();

		// инициализация была выполнена в getTabHost
		// метод setup вызывать не нужно

		TabHost.TabSpec tabSpec;

		tabSpec = tabHost.newTabSpec("tag1");
		tabSpec.setIndicator("Заявки-Регион");
		tabSpec.setContent(new Intent(this, RequestsListRegional.class));
		tabHost.addTab(tabSpec);

		tabSpec = tabHost.newTabSpec("tag2");
		tabSpec.setIndicator("Мои заявки");
		tabSpec.setContent(new Intent(this, RequestsListPersonal.class));
		tabHost.addTab(tabSpec);

	}

    // Событие при создании меню с опциями
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		menu.add("Обновить");
		return super.onCreateOptionsMenu(menu);
	}

    // Событие при выборе элемениа меню(смена активити)
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getTitle() == "Обновить") {
			Intent intent = getIntent();
			overridePendingTransition(0, 0);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			finish();
			overridePendingTransition(0, 0);
			startActivity(intent);

		} else {
			return false;
		}
		return true;
	}

}
