package com.chebdev.m_servicedesk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ConnectionDetector.ConnectionDetector;
import XMLParser.XMLParser;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.net.Uri;

// Класс отображения конкретной заявки
public class RequestDescription extends Activity {

    private TextView requestNumber;
    private TextView typeDescription;
    private TextView priority;
    private TextView registeredDescription;
    private TextView clientDescription;
    private TextView addressDescription;
    private TextView limitDateDescription;
    private TextView defectDescription;
    private Button doneButton;
    private Button asignButton;

    // флаг статуса соединения с интернетом
    Boolean isInternetPresent = false;
    // экземпляр класса проверки соединения
    ConnectionDetector cd;

    public String URL;
    public String currentStateID;
    public String engineer_Id;

    // элементы XML
    static final String REQUEST = "request"; // parent node
    public String REQUEST_ID;
    static final String REQUEST_TYPE = "requesttypetitle";
    static final String CLIENT_NAME = "clienttitle";
    static final String CLIENT_ADDRESS = "locationtitle";
    static final String STATE_ID = "stateid";
    static final String STATE_TITLE = "state_title";
    static final String REQUEST_DESC = "freedescription";
    static final String REGISTRATION_DATE = "registrationdate";
    static final String LIMIT_DATE = "limitdate";

    // метод срабатываемый при создании активити
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_description_layout);
        setupVariables();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // экземпляр класса проверки соединения
        cd = new ConnectionDetector(getApplicationContext());

        // статус подключения интернета
        isInternetPresent = cd.isConnectingToInternet();

        // если есть интернет
        if (isInternetPresent) {
            XMLParser parser = new XMLParser();
            // String xml = parser.getXmlFromUrl(URL); // getting XML

            // Getting xml to String by HTTP Get
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(URL);
            ResponseHandler<String> handler = new BasicResponseHandler();
            String xml = "";
            try {
                xml = client.execute(request, handler);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Document doc = parser.getDomElement(xml); // getting DOM element

            NodeList nl = doc.getElementsByTagName(REQUEST);
            // looping through all item nodes <item>
            for (int i = 0; i < nl.getLength(); i++) {

                Element e = (Element) nl.item(i);

                // requestNumber.setText(parser.getValue(e, REQUEST_ID));
                requestNumber.setText("Заявка:" + REQUEST_ID);
                currentStateID = parser.getValue(e, STATE_ID);
                typeDescription.setText(parser.getValue(e, REQUEST_TYPE));
                registeredDescription.setText(parser.getValue(e, REGISTRATION_DATE));
                clientDescription.setText(parser.getValue(e, CLIENT_NAME));
                addressDescription.setText(parser.getValue(e, CLIENT_ADDRESS));
                limitDateDescription.setText(parser.getValue(e, LIMIT_DATE));
                defectDescription.setText(parser.getValue(e, REQUEST_DESC));
            }
            String[] priorities = {"Высокая", "Средняя", "Низкая"};
            int idx = new Random().nextInt(priorities.length);
            String random = (priorities[idx]);
            priority.setText(random);
        } else {
            Toast.makeText(getApplicationContext(), "Проверьте ваше интернет соединение", Toast.LENGTH_SHORT).show();
        }
    }

    // взятие заявки в работу(отправка запроса на сервер)
    public void postAssignedToData() {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();

		/* login.php returns true if username and password is equal to saranga */
        HttpPost httppost = new HttpPost("http://helpdesk.mserv21.ru/api/take_request/" + REQUEST_ID);

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("assigned_to", engineer_Id));
            nameValuePairs.add(new BasicNameValuePair("state", "2"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            Log.w("SENCIDE", "Execute HTTP Post Request");
            HttpResponse response = httpclient.execute(httppost);

            String str = inputStreamToString(response.getEntity().getContent())
                    .toString();
            Log.w("SENCIDE", str);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Конвертация потока в строку
    private StringBuilder inputStreamToString(InputStream is) {
        String line = "";
        StringBuilder total = new StringBuilder();
        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        // Read response until the end
        try {
            while ((line = rd.readLine()) != null) {
                total.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Return full string
        return total;
    }

    // Метод для кнопки Выполнено
    public void doneRequest(View view) {

        if (!currentStateID.equals("2")) {
            Toast.makeText(getApplicationContext(), "Перед выполнением,возьмите заявку в работу.",
                    Toast.LENGTH_SHORT).show();
        } else {
            Log.w("Time", currentStateID);
            Intent intentDone = new Intent(this, RequestDone.class);
            intentDone.putExtra("requestIDtoDone", REQUEST_ID);
            startActivity(intentDone);
        }
    }

    // Метод для кнопки В работу
    public void asignRequest(View view) {
        if (!currentStateID.equals("1")) {
            Toast.makeText(getApplicationContext(), "Действие не возможно! Заявка уже назначена.",
                    Toast.LENGTH_SHORT).show();
        } else {
            postAssignedToData();
            Intent intent = new Intent(this, RequestsListParentActivity.class);
            startActivity(intent);
        }
    }

    public void callContact(View view) {
        String numb = "tel:+700000000";
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(numb));
        startActivity(intent);
    }

    // установка ссылок на элементы интерфейса
    private void setupVariables() {
        requestNumber = (TextView) findViewById(R.id.requestNumberDescription);
        typeDescription = (TextView) findViewById(R.id.typeDescription);
        priority = (TextView) findViewById(R.id.priorityDescription);
        registeredDescription = (TextView) findViewById(R.id.registeredDescription);
        clientDescription = (TextView) findViewById(R.id.clientDescription);
        addressDescription = (TextView) findViewById(R.id.addressDescription);
        limitDateDescription = (TextView) findViewById(R.id.limitDateDescription);
        defectDescription = (TextView) findViewById(R.id.defectDescription);
        doneButton = (Button) findViewById(R.id.doneButton);
        asignButton = (Button) findViewById(R.id.assignButton);
        Intent intentFromPreviosActivity = getIntent();
        REQUEST_ID = intentFromPreviosActivity
                .getStringExtra("requestIDtoDescription");
        URL = "http://helpdesk.mserv21.ru/api/requests/?format=xml&fields=request_id,state_id,request_type_title,registration_date,client_title,location_title,limit_date,free_description&id=" + REQUEST_ID;
        SharedPreferences pref = getApplicationContext().getSharedPreferences("EngineerData", 0);
        engineer_Id = pref.getString("user_id", null);

    }


}
