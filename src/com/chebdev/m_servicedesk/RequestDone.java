package com.chebdev.m_servicedesk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ConnectionDetector.ConnectionDetector;
import XMLParser.XMLParser;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

// класс отображения активити выполненной заявки
public class RequestDone extends Activity {

    public int startYear, startMonth, startDay, mHour, mMinute, endYear, endMonth, endDay;
    public String engineer_Id;
    public String currentStateID;
    private EditText worksDoneDescription;
    private Spinner objectStatusSpinner;
    private Button startDate;
    private Button startTime;
    private Button finishDate;
    private Button finishTime;
    private String currentRequestID;
    private String doneWorkDescription;
    private String doneFreeDescription = null;
    private String doneStartDate;
    private String doneStartTime;
    private String doneEndDate;
    private String donEndTime;
    final static String URLDescription = "http://helpdesk.mserv21.ru/mobile_api/catalog/request_descriptions";
    final static String URLFinishedState = "http://helpdesk.mserv21.ru/mobile_api/catalog/request_finished_state";
    static final String REQUEST = "request"; // parent node
    static final String DESCRIPTION_TITLE = "title";
    static final String STATUS_TITLE = "title";
    public String serverResponse;
    public Integer endPosDescription;
    public Integer endPosStatus;
    private String doneObjectStatus;
    public Boolean workDeskConfirm = false;
    public Boolean dateConfirm = false;
    public Boolean objectStatuConfirm = false;
    public Boolean confirm = false;
    Boolean isInternetPresent = false;
    static final String PARENT = "data"; // parent node
    static final String SUCCESS = "success";
    ConnectionDetector cd;


    // метод срабатываемый при создании активити
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_done_layout);
        setupVariables();
        GetXMLStatus();
        GetXMLDescription();
    }

    // установка даты старта выполнения заявки
    public void setStartDate(View view) {

        Calendar c = Calendar.getInstance();
        startYear = c.get(Calendar.YEAR);
        startMonth = c.get(Calendar.MONTH);
        startDay = c.get(Calendar.DAY_OF_MONTH);

        // Launch Date Picker Dialog
        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // Display Selected date in button
                        startDate.setText(year + "-" + (monthOfYear + 1) + "-"
                                + dayOfMonth);
                        startYear = year;
                        startMonth = monthOfYear;
                        startDay = dayOfMonth;
                    }
                }, startYear, startMonth, startDay);
        dpd.show();

    }

    // установка времени старта выполнения заявки
    public void setStartTime(View view) {
        // Process to get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        // Display Selected time in textbox
                        startTime.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        tpd.show();

    }

    // установка даты окончания выполнения заявки
    public void setFinishDate(View view) {
        // Process to get Current Date
        final Calendar c = Calendar.getInstance();
        endYear = c.get(Calendar.YEAR);
        endMonth = c.get(Calendar.MONTH);
        endDay = c.get(Calendar.DAY_OF_MONTH);

        // Launch Date Picker Dialog
        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // Display Selected date in button
                        finishDate.setText(year + "-" + (monthOfYear + 1) + "-"
                                + dayOfMonth);
                        endYear = year;
                        endMonth = monthOfYear;
                        endDay = dayOfMonth;
                    }
                }, endYear, endMonth, endDay);
        dpd.show();

    }

    // запрос для получения статуса заявки
    public void GetXMLStatus() {
        XMLParser parser = new XMLParser();

        // Getting xml to String by HTTP Get
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(URLFinishedState);
        ResponseHandler<String> handler = new BasicResponseHandler();
        String xml = "";
        try {
            xml = client.execute(request, handler);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // getting DOM element
        Document doc = parser.getDomElement(xml);
        NodeList nl = doc.getElementsByTagName(REQUEST);
        String[] objectDescriptionArray = new String[nl.getLength() + 1];

        // looping through all item nodes <item>
        for (int i = 0; i < nl.getLength(); i++) {
            // creating new HashMap
            Element e = (Element) nl.item(i);
            // adding to array
            objectDescriptionArray[i] = parser.getValue(e, STATUS_TITLE);
        }
        endPosStatus = nl.getLength();
        objectDescriptionArray[endPosStatus] = "Выберите статус заявки";
        // адаптер
        ArrayAdapter<String> adapterStatus = new ArrayAdapter<String>(this,
                R.layout.spinner_item, objectDescriptionArray);
        adapterStatus
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        objectStatusSpinner.setAdapter(adapterStatus);
        // выделяем элемент

        objectStatusSpinner.setSelection(endPosStatus);
        // устанавливаем обработчик нажатия
        objectStatusSpinner
                .setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                    }
                });

    }

    // получения справочника готовых описаний заявок
    public void GetXMLDescription() {
        XMLParser parser = new XMLParser();

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(URLDescription);
        ResponseHandler<String> handler = new BasicResponseHandler();
        String xml = "";
        try {
            xml = client.execute(request, handler);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Document doc = parser.getDomElement(xml);
        NodeList nl = doc.getElementsByTagName(REQUEST);
        String[] objectStatusArray = new String[nl.getLength() + 1];

        for (int i = 0; i < nl.getLength(); i++) {
            Element e = (Element) nl.item(i);
            objectStatusArray[i] = parser.getValue(e, DESCRIPTION_TITLE);
        }
        endPosDescription = nl.getLength();
        objectStatusArray[endPosDescription] = "Выберите описание объекта";
        Log.w("Temp", String.valueOf(endPosDescription));
        // адаптер
        ArrayAdapter<String> adapterStatus = new ArrayAdapter<String>(this,
                R.layout.spinner_item, objectStatusArray);
        adapterStatus
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    // установка времени финиша выполнения заявки
    public void setFinishTime(View view) {
        // Process to get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        // Display Selected time in textbox
                        finishTime.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        tpd.show();

    }
    // отправка запроса на сервер при выполнении заявки
    public void postDoneData() {
        if (!worksDoneDescription.getText().toString().equals("")) {
            doneFreeDescription = worksDoneDescription.getText().toString();
            workDeskConfirm = true;
        }  

        if (endPosStatus.equals(objectStatusSpinner.getSelectedItemPosition())) {
            Toast.makeText(getApplicationContext(),
                    "Выберите статус объекта после ремонта!",
                    Toast.LENGTH_SHORT).show();
            objectStatuConfirm = false;

        } else {
            doneObjectStatus = String.valueOf(objectStatusSpinner
                    .getSelectedItemPosition() + 1);
            objectStatuConfirm = true;
        }

        doneStartDate = startDate.getText().toString();
        doneStartTime = startTime.getText().toString();
        doneEndDate = finishDate.getText().toString();
        donEndTime = finishTime.getText().toString();

        if (doneStartDate.equals("Дата") || doneEndDate.equals("Дата")
                || doneStartTime.equals("Время") || donEndTime.equals("Время")) {
            Toast.makeText(getApplicationContext(),
                    "Заполните все поля Даты и Времени", Toast.LENGTH_SHORT)
                    .show();
            dateConfirm = false;

        } else {
            String postStartDate = doneStartDate + " " + doneStartTime;
            String postEndDate = doneEndDate + " " + donEndTime;
            dateConfirm = true;

            if (workDeskConfirm && objectStatuConfirm && dateConfirm) {
                // Create a new HttpClient and Post Header
                HttpClient httpclient = new DefaultHttpClient();
                HttpPut httppost = new HttpPut(
                        "http://helpdesk.mserv21.ru/api/requests/?format=xml");

                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
                            6);
                    nameValuePairs.add(new BasicNameValuePair("id",
                            currentRequestID));
                    nameValuePairs.add(new BasicNameValuePair("assigned_to",
                            engineer_Id));
                    nameValuePairs.add(new BasicNameValuePair("state", "3"));
                    nameValuePairs.add(new BasicNameValuePair(
                            "free_description", doneFreeDescription));
                    nameValuePairs.add(new BasicNameValuePair("description",
                            doneWorkDescription));
                    nameValuePairs.add(new BasicNameValuePair("start_date",
                            postStartDate));
                    nameValuePairs.add(new BasicNameValuePair("finish_date",
                            postEndDate));
                    nameValuePairs.add(new BasicNameValuePair("finished_state",
                            doneObjectStatus));

                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    // Execute HTTP Post Request
                    Log.w("SENCIDE", "Execute HTTP Post Request");
                    HttpResponse response = httpclient.execute(httppost);

                    String str = inputStreamToString(
                            response.getEntity().getContent()).toString();
                    Log.w("Temp", str);

                    XMLParser parser = new XMLParser();
                    Document doc = parser.getDomElement(str); // getting DOM element
                    NodeList nl = doc.getElementsByTagName(PARENT);
                    Element e = (Element) nl.item(0);
                    String serverResponse = parser.getValue(e, SUCCESS);
                    Log.w("Temp", serverResponse);

                    if (serverResponse.equals("1")) {
                        Toast.makeText(getApplicationContext(),
                                "Данные успешно сохранены", Toast.LENGTH_SHORT)
                                .show();
                        confirm = true;
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Данные не сохранены!", Toast.LENGTH_SHORT)
                                .show();
                        confirm = false;
                    }

                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // Конвертация потока в строку
    private StringBuilder inputStreamToString(InputStream is) {
        String line = "";
        StringBuilder total = new StringBuilder();
        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        // Read response until the end
        try {
            while ((line = rd.readLine()) != null) {
                total.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Return full string
        return total;
    }

    // Сохранение заявки
    public void saveRequest(View view) {
        if (startYear > endYear) {
            Toast.makeText(getApplicationContext(),
                    "Дата окончания не может быть меньше даты начала!",
                    Toast.LENGTH_SHORT).show();
        } else if (startMonth > endMonth) {
            Toast.makeText(getApplicationContext(),
                    "Дата окончания не может быть меньше даты начала!",
                    Toast.LENGTH_SHORT).show();

        } else if (startDay > endDay) {
            Toast.makeText(getApplicationContext(),
                    "Дата окончания не может быть меньше даты начала!",
                    Toast.LENGTH_SHORT).show();
        } else {
            postDoneData();

        }

        if (confirm) {
            Intent intent = new Intent(this, RequestsListParentActivity.class);
            startActivity(intent);
        }

    }

    // установка ссылок на элементы интерфейса
    private void setupVariables() {
        Intent intent = getIntent();
        currentRequestID = intent.getStringExtra("requestIDtoDone");
        worksDoneDescription = (EditText) findViewById(R.id.doneWorkET);
        objectStatusSpinner = (Spinner) findViewById(R.id.objectStatusSpiner);
        startDate = (Button) findViewById(R.id.setStartDate);
        startTime = (Button) findViewById(R.id.setStartTime);
        finishDate = (Button) findViewById(R.id.setFinishDate);
        finishTime = (Button) findViewById(R.id.setFinishTime);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "EngineerData", 0);
        engineer_Id = pref.getString("user_id", null);
    }
}
