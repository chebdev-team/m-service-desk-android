package com.chebdev.m_servicedesk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ConnectionDetector.ConnectionDetector;
import XMLParser.XMLParser;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

// класс отображения активити входа в систему
public class MainActivity extends Activity {

    private EditText username;
    private EditText password;
    private Button login;
    private TextView loginLockedTV;
    private String loginName;
    private String loginPassword;
    public String engineer_Id;
    public String region_Id;

    static final String DATA = "data"; // parent node
    static final String ENGINEER_ID_FROM_XML = "userid";
    static final String REGION_ID_FROM_XML = "regionid";

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    // метод срабатываемый при создании активити
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_main);
        setupVariables();
    }

    // вход в приложение
    public void postLoginData() {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();

        // инициализация запроса для проверки логина и пароля
        HttpPost httppost = new HttpPost("http://helpdesk.mserv21.ru/api/mobile_login");

        try {
            // логин и пароль с текстовых полей
            loginName = username.getText().toString();
            loginPassword = password.getText().toString();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("login", loginName));
            nameValuePairs.add(new BasicNameValuePair("password", loginPassword));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // отправка запроса к серверу
            Log.w("SENCIDE", "Execute HTTP Post Request");
            HttpResponse response = httpclient.execute(httppost);

            String str = inputStreamToString(response.getEntity().getContent()).toString();
            Log.w("SENCIDE", str);

            // проверка ответа от сервера, правильные ли логин и пароль
            if (str.toString().equalsIgnoreCase("false")) {
                Log.w("SENCIDE", "FALSE");
                Toast.makeText(getApplicationContext(), "Не верные учетные данные!", Toast.LENGTH_SHORT).show();
            } else {
                // если логин и пароль верны меняем активити на "список заявок"
                XMLParser parser = new XMLParser();
                Document doc = parser.getDomElement(str); // getting DOM element
                NodeList nl = doc.getElementsByTagName(DATA);
                // цикл проходящий по элементам полученного XML
                for (int i = 0; i < nl.getLength(); i++) {
                    Element e = (Element) nl.item(i);
                    // adding each child node to HashMap key => value
                    engineer_Id = parser.getValue(e, ENGINEER_ID_FROM_XML);
                    region_Id = parser.getValue(e, REGION_ID_FROM_XML);
                }
                SharedPreferences.Editor editor = getSharedPreferences("EngineerData", MODE_PRIVATE).edit();
                editor.putString("region_id", region_Id);
                editor.putString("user_id", engineer_Id);
                editor.commit();
                Toast.makeText(getApplicationContext(), "Добро пожаловать!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, RequestsListParentActivity.class);
                startActivity(intent);
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Конвертация потока в строку
    private StringBuilder inputStreamToString(InputStream is) {
        String line = "";
        StringBuilder total = new StringBuilder();
        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        // Read response until the end
        try {
            while ((line = rd.readLine()) != null) {
                total.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Return full string
        return total;
    }

    // аутентификация пользователя
    public void authenticateLogin(View view) {
        // creating connection detector class instance
        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();
        // check for Internet status
        if (isInternetPresent) {
            postLoginData();
        } else {
            Toast.makeText(getApplicationContext(), "Проверьте ваше интернет соединение", Toast.LENGTH_SHORT).show();
        }
    }

    // установка ссылок на элементы интерфейса
    private void setupVariables() {
        username = (EditText) findViewById(R.id.loginETUsername);
        password = (EditText) findViewById(R.id.loginETPassword);
        login = (Button) findViewById(R.id.loginBtn);
    }

}