package com.chebdev.m_servicedesk;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ConnectionDetector.ConnectionDetector;
import XMLParser.XMLParser;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

//import android.renderscript.Element;

// класс отображения списка региональных заявок
public class RequestsListRegional extends ListActivity {

    private String URL;
    private String region_Id;

    static final String REQUEST = "request"; // parent node
    static final String REQUEST_ID = "requestid";
    static final String CLIENT_NAME = "clienttitle";
    static final String CLIENT_ADDRESS = "locationtitle";
    static final String REQUEST_DESC = "freedescription";
    static final String ASSIGNED = "username";

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    // метод срабатываемый при создании активити(проверка интернет соединения, запрос к серверу для получения региональных заявок, парсинг XML, вывод заявок)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("EngineerData", 0);
        region_Id = pref.getString("region_id", null);
        URL = "http://helpdesk.mserv21.ru/api/requests/?format=xml&fields=request_id,client_title,location_title,free_description,username&state_id=1,2&region_id=" + region_Id;


        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();

        // если есть интернет соединение
        if (isInternetPresent) {
            ArrayList<HashMap<String, String>> menuItems = new ArrayList<HashMap<String, String>>();
            XMLParser parser = new XMLParser();
            //String xml = parser.getXmlFromUrl(URL); // getting XML

            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(URL);
            ResponseHandler<String> handler = new BasicResponseHandler();
            String xml = "";
            try {
                xml = client.execute(request, handler);
            } catch (IOException e) {
                e.printStackTrace();
            }


            Document doc = parser.getDomElement(xml); // getting DOM element
            NodeList nl = doc.getElementsByTagName(REQUEST);
            // looping through all item nodes <item>
            for (int i = 0; i < nl.getLength(); i++) {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                Element e = (Element) nl.item(i);
                // adding each child node to HashMap key => value
                map.put(REQUEST_ID, "Заявка: " + parser.getValue(e, REQUEST_ID));
                map.put(CLIENT_NAME,
                        "Клиент: " + parser.getValue(e, CLIENT_NAME));
                map.put(CLIENT_ADDRESS,
                        "Адрес: " + parser.getValue(e, CLIENT_ADDRESS));
                map.put(REQUEST_DESC, parser.getValue(e, REQUEST_DESC));
                map.put(ASSIGNED, parser.getValue(e, ASSIGNED));
                // adding HashList to ArrayList
                menuItems.add(map);
            }

            // Добавление элементов в ListView

            ListAdapter adapter = new SimpleAdapter(this, menuItems,
                    R.layout.list_item, new String[]{REQUEST_ID, CLIENT_NAME, CLIENT_ADDRESS,
                    REQUEST_DESC, ASSIGNED}, new int[]{R.id.request_id,
                    R.id.client_name, R.id.client_address, R.id.request_desciption, R.id.asigned}) {

                @Override
                public View getView(int position, View convertView,
                                    ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    //view.setBackgroundColor(0xFFFFFFFF);
                    view.setBackgroundResource(R.drawable.selector);
                    if (!((TextView) view.findViewById(R.id.asigned))
                            .getText().toString().equals("")) {

                        view.setBackgroundResource(R.drawable.selector2);

                    }

                    return view;
                }

            };

            setListAdapter(adapter);


        } else {
            Toast.makeText(getApplicationContext(), "Проверьте ваше интернет соединение", Toast.LENGTH_SHORT).show();
        }
    }
    // метод срабатываемый при выборе заявки(смена активити)
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(this, RequestDescription.class);
        String requestIDValueFromView = ((TextView) v.findViewById(R.id.request_id))
                .getText().toString();
        v.performClick();
        intent.putExtra("requestIDtoDescription", requestIDValueFromView.substring(8, requestIDValueFromView.length()));
        startActivity(intent);
    }
}
